# -*- coding: utf-8 -*-
from flask import Flask
from flask import  request, jsonify, make_response
import logging
#from edtf_validate.valid_edtf import is_valid
from edtf import parse_edtf
from elasticapm.contrib.flask import ElasticAPM
from elasticapm.handlers.logging import LoggingHandler
from edtf.parser.edtf_exceptions import EDTFParseException 
app = Flask(__name__)
app.config['SECRET_KEY'] = 'mo00 says the GNU!'
app = Flask('api')
app.config['ELASTIC_APM'] ={
    'SERVICE_NAME': 'validEDTF-dev',
    'SERVER_URL': 'http://apm-server-prd.apps.do-prd-okp-m0.do.viaa.be:80'
}
apm = ElasticAPM(app,logging=True)


@app.route("/edtf/isValid.json/", methods=['GET'])
def date_check():
    if 'date' in request.args:
        to_parse=request.args['date']
    else:
        out ={"error": 'No date args found'}
        app.logger.error( 'not a valid argument', exc_info=True)
        return make_response(jsonify(out), 400)
    if is_valid(to_parse):
        d = is_valid(to_parse)
        out ={"validEDTF": True}
        apm.capture_message('date type: %s' %d)
        app.logger.info( 'date type: %s' %d, exc_info=True)
        return make_response(jsonify(out), 200) #

    else :
        out ={"validEDTF": False}
        apm.capture_message('not a EDTF format')
        app.logger.error( 'not a valide date', exc_info=True)
        return make_response(jsonify(out), 400)
    return None


def is_valid(date):
    try:
        d = parse_edtf(date)
        if d:
           app.logger.info('date result: %s' %d)
    except EDTFParseException as e:
        apm.capture_exception()
        app.logger.error( 'not a valid EDTF date %s' %date, exc_info=True)
        return False
    return d

if __name__ == '__main__':
    app.debug = False
    handler = LoggingHandler(client=apm.client)
    handler.setLevel(logging.ERROR)
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    app.logger.addHandler(handler)
    app.logger.addHandler(ch)
    app.run(port=8080,host='0.0.0.0')

