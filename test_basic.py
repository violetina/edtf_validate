import os
import unittest
 
from app import app
 
 
import xmlrunner
xml_report_dir = "./test_results"
#testRunner=xmlrunner.XMLTestRunner(output=xml_report_dir)
 
class BasicTests(unittest.TestCase):
 
    ############################
    #### setup and teardown ####
    ############################
 
    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False

        self.app = app.test_client()
        #db.drop_all()
        #db.create_all()
 
       
        self.assertEqual(app.debug, False)
 
    # executed after each test
    def tearDown(self):
        pass
 
 
###############
#### tests ####
###############
 
    def test_main_page(self):
        response = self.app.get('/edtf/isValid.json/?date=1966', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        
    def test_invalide(self):
        response = self.app.get('/edtf/isValid.json/?date=19666', follow_redirects=True)
        self.assertEqual(response.status_code, 400)    


# Run all test functions.        
def run_all_test():
    # Run all test functions.
    unittest.main()
# Run all test function and generate html report.    
def run_all_test_generate_xml_report():    
    # Run all test functions with HtmlTestRunner to generate html test report.
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output=xml_report_dir)) 
 
if __name__ == "__main__":
    
    run_all_test_generate_xml_report()
   # unittest.main()