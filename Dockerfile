#FROM python@sha256:06e58d83a391f85a61b4348b93294b1ac2ae7fdb6d0a282804b1c634f9e45f7e
FROM alpine:3.9
EXPOSE 8080
VOLUME /usr/src/app/public
WORKDIR /usr/src/app
RUN apk add --no-cache \
        python3 \
        python3-dev build-base linux-headers pcre-dev
#pip install uwsgi
ENV USER=developer
ENV UID=12006
ENV GID=12006

RUN addgroup --gid "$GID" "$USER" \
    && adduser \
    --disabled-password \
    --gecos "" \
    --home "$(pwd)" \
    --ingroup "$USER" \
    --no-create-home \
    --uid "$UID" \
    "$USER"

COPY . .
RUN rm -rf public/*
RUN pip3 install --no-cache-dir uwsgi edtf flask elastic-apm[flask]==4.2.0
#CMD [ "uwsgi", "--socket", "0.0.0.0:8080", \
#               "--uid", "uwsgi", \
#               "--plugins", "python3", \
#               "--protocol", "uwsgi", \
#               "--wsgi", "main:application" ]

#RUN pip install edtf flask elastic-apm[flask]==4.2.0
#WORKDIR /app
USER developer

EXPOSE 8080
ADD ./app.py /app/app.py
ADD ./uwsgi.ini /app/uwsgi.ini
ADD ./app_uwsgi.py /app/app_uwsgi.py
CMD  uwsgi --plugin python3 --manage-script-name --mount /=app uwsgi.ini 
#CMD python3 app.py
